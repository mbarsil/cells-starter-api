var express = require('express');
var app = express();

var express = require('express');
var app = express();

var movies = require('./movies.json');
var events = require('./events.json');

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    else {
      next();
    }
};


app.set('port', (process.env.PORT || 5000));

app.use(allowCrossDomain);

app.get('/movies', function(request, response) {
  response.json(movies);
});

app.get('/events', function(request, response) {
  response.json(events);
});


app.use(function(req, res){
    res.send(404);
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});